package glib.cluster.Book.Service.Implements;

import glib.cluster.AAA.Common.RException;
import glib.cluster.Book.Controller.Model.book_model;
import glib.cluster.Book.Entity.Book;
import glib.cluster.Book.Service.Interface.bookInterface;
import glib.cluster.Book.Service.Repository.bookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class bookImpl implements bookInterface {
    @Autowired
    private bookRepository bookRepository;
    @Transactional
    @Override
    public Book addbook(Book book) throws RException {
        if(book.getBno()==null||book.getBno()=="")
            throw new RException(400);
        if(bookRepository.findByBno(book.getBno())!=null)
            throw new RException(401);
        return bookRepository.save(book);
    }

}
