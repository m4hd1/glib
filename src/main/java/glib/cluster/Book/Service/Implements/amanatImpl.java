package glib.cluster.Book.Service.Implements;

import glib.cluster.AAA.Common.RException;
import glib.cluster.Book.Entity.Amanat;
import glib.cluster.Book.Service.Interface.amanatInterface;
import glib.cluster.Book.Service.Repository.amanatRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class amanatImpl implements amanatInterface {
    @Autowired
    private amanatRepository amanatRepository;

    @Transactional
    @Override
    public Amanat setamanat(Amanat amanat)throws RException
    {
        if(amanat.getBook().getIsrefrece()==true)
            throw new RException(400);
        if(amanatRepository.findByBook(amanat.getBook())!=null&&amanatRepository.findByBook(amanat.getBook()).getIsreturned()==false)
            throw new RException(401);
        return  amanatRepository.save(amanat);
    }
}
