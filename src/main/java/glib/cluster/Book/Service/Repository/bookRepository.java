package glib.cluster.Book.Service.Repository;

import glib.cluster.Book.Entity.Book;
import org.springframework.data.jpa.repository.JpaRepository;

public interface bookRepository extends JpaRepository<Book,String> {
    Book findByBno(String bno);
}
