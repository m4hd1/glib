package glib.cluster.Book.Service.Repository;

import glib.cluster.Book.Entity.Amanat;
import glib.cluster.Book.Entity.Book;
import org.springframework.data.jpa.repository.JpaRepository;

public interface amanatRepository extends JpaRepository<Amanat,String> {
    Amanat findByBook(Book book);
}
