package glib.cluster.Book.Service.Interface;

import glib.cluster.AAA.Common.RException;
import glib.cluster.Book.Controller.Model.book_model;
import glib.cluster.Book.Entity.Book;

import javax.transaction.Transactional;

public interface bookInterface {
    @Transactional
    Book addbook(Book book)throws RException;
}
