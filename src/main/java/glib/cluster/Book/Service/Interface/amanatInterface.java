package glib.cluster.Book.Service.Interface;

import glib.cluster.AAA.Common.RException;
import glib.cluster.Book.Entity.Amanat;

import javax.transaction.Transactional;

public interface amanatInterface {
    @Transactional
    Amanat setamanat(Amanat amanat)throws RException;
}
