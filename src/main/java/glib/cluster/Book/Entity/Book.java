package glib.cluster.Book.Entity;

import glib.cluster.AAA.Entity.User;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
@Getter
@Setter

@Entity
@Table(name = "book")
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    @NotNull
    private String bno;

    @Column
    @NotNull
    private String bookname;

    @Column
    @NotNull
    private Boolean isrefrece;

    @Column
    @NotNull
    private String description;

    @ManyToOne
    @JoinColumn(name = "userid",nullable = false)
    private User user;

}
