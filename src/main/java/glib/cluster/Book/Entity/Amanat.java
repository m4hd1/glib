package glib.cluster.Book.Entity;

import glib.cluster.AAA.Entity.User;
import lombok.Getter;
import lombok.Setter;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;


@Getter
@Setter
@Entity
@Table(name = "amanat")
public class Amanat {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create-at")
    private Date createdate;

    @Column
    private Boolean isreturned;

    @ManyToOne
    @JoinColumn(name = "book_id",nullable = false)
    private Book book;


    @ManyToOne
    @JoinColumn(name = "user_id",nullable = false)
    private User user;



}
