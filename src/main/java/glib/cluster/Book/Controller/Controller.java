package glib.cluster.Book.Controller;

import glib.cluster.AAA.Common.RException;
import glib.cluster.AAA.Service.Repository.UserRepository;
import glib.cluster.Book.Controller.Model.book_model;
import glib.cluster.Book.Entity.Amanat;
import glib.cluster.Book.Entity.Book;
import glib.cluster.Book.Service.Interface.amanatInterface;
import glib.cluster.Book.Service.Interface.bookInterface;
import glib.cluster.Book.Service.Repository.bookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
public class Controller {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private bookInterface bookInterface;
    @Autowired
    private bookRepository bookRepository;
    @Autowired
    private amanatInterface amanatInterface;



    @PreAuthorize("hasRole('USER') ")
    @RequestMapping(value = "/getMsg",method = RequestMethod.GET)
    public String  greeting()
    {
        return "spring sec example";
    }

    @PreAuthorize("hasRole('USER') ")
    @RequestMapping(value = "/addbook",method = RequestMethod.POST)
    public ResponseEntity<?> addbook(@RequestBody book_model book_model){
        Book book=new Book();
        book.setBno(book_model.getBno());
        book.setBookname(book_model.getBookname());
        book.setDescription(book_model.getDescription());
        book.setIsrefrece(book_model.getIsrefrece());
        book.setUser(userRepository.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName()).get());
        try {
            bookInterface.addbook(book);
            return new ResponseEntity<Integer>(200, HttpStatus.OK);
        } catch (RException e) {
            return new ResponseEntity<Integer>(e.getStatus(), HttpStatus.BAD_REQUEST);
        }
    }
    @PreAuthorize("hasRole('USER') ")
    @RequestMapping(value = "/getbooks",method = RequestMethod.GET)
    public ResponseEntity<?>getallbooks(@RequestParam String bno)
    {
//        List<Book> books=bookRepository.findAll();
//        List<book_model>book_models=new ArrayList<>();
//        for(Book x:books)
//        {
//            book_model book_model=new book_model();
//            book_model.setBno(x.getBno());
//            book_model.setBookname(x.getBookname());
//            book_model.setDescription(x.getDescription());
//            book_model.setIsrefrece(x.getIsrefrece());
//            book_models.add(book_model);
//        }
        if( bookRepository.findByBno(bno)==null)
            return new ResponseEntity<>("کتاب با مشخصات فوق موجود نیست", HttpStatus.BAD_REQUEST);
            Book book=bookRepository.findByBno(bno);
            book_model book_model=new book_model();
           book_model.setBno(book.getBno());
           book_model.setBookname(book.getBookname());
           book_model.setDescription(book.getDescription());
          book_model.setIsrefrece(book.getIsrefrece());

        return new ResponseEntity<>(book_model, HttpStatus.OK);
    }
    @PreAuthorize("hasRole('USER') ")
    @RequestMapping(value = "/setamanat",method = RequestMethod.POST)
  public ResponseEntity<?> setamanat(@RequestParam String bno)
    {
        Amanat amanat=new Amanat();
        amanat.setCreatedate(new Date());
        amanat.setIsreturned(false);
        amanat.setBook(bookRepository.findByBno(bno));
        amanat.setUser(userRepository.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName()).get());
        try {
            amanatInterface.setamanat(amanat);
            return new ResponseEntity<>(200, HttpStatus.OK);
        } catch (RException e) {
            return new ResponseEntity<>(e.getStatus(), HttpStatus.BAD_REQUEST);
        }

    }




}
