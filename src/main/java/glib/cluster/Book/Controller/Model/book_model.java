package glib.cluster.Book.Controller.Model;

import glib.cluster.AAA.Entity.User;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class book_model {

    private String bno;
    private String bookname;
    private Boolean isrefrece;
    private String description;
}
