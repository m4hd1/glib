package glib.cluster.AAA.Common;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RException extends Exception {

    private Integer status;

    public RException()
    {

    }
    public RException(Integer status)
    {
        super();
        this.status=status;
    }
    public RException(String massege,Integer status)
    {
        super(massege);
        this.status=status;
    }
}
