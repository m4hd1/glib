package glib.cluster.AAA.Common;

public enum Roles {
	ROLE_ADMIN(1), ROLE_USER(2), ROLE_SUPERADMIN(3);
	private int value;

	Roles(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public static String GetRole(Integer i)
	{
		if (i==1)
			return "ROLE_ADMIN";
		else if (i==2)
			return "ROLE_USER";
		else if (i==3)
			return "ROLE_TEST";
		return "";
	}

}
 