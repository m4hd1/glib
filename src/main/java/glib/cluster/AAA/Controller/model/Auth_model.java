package glib.cluster.AAA.Controller.model;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@Getter
@Setter
@ToString
public class Auth_model {


    @NotBlank
    private String password;
    @NotBlank
    @Email
    private String email;

    private Integer role;
}
