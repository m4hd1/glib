package glib.cluster.AAA.Controller.model;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
@ToString
public class login_model {

    @NotBlank
    private String username,password;
}
