package glib.cluster.AAA.Controller;

import glib.cluster.AAA.Entity.User;
import glib.cluster.AAA.Service.Repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;

@Component
@Scope(value = "session", proxyMode = ScopedProxyMode.TARGET_CLASS)
@Controller
public class GlobalController {

	@Autowired
	private UserRepository userService;

	private User loginUser;

	public User getLoginUser() {
		if (loginUser == null) {
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			loginUser = userService.findByUsername(auth.getName()).get();
		}
		return loginUser;
	}

}
