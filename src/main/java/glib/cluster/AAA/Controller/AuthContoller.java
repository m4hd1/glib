package glib.cluster.AAA.Controller;


import glib.cluster.AAA.Common.Cookieutill;
import glib.cluster.AAA.Common.RException;
import glib.cluster.AAA.Controller.model.Auth_model;
import glib.cluster.AAA.Controller.model.login_model;
import glib.cluster.AAA.Entity.User;
import glib.cluster.AAA.Service.Interface.UserInterface;
import glib.cluster.AAA.Service.Repository.UserRepository;
import glib.cluster.AAA.security.JwtAuthenticationFilter;
import glib.cluster.AAA.security.JwtTokenProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.Size;
import java.io.IOException;
import java.util.Optional;


@Component
@RestController
@RequestMapping("/auth")
public class AuthContoller {


    private static final String jwtTokenCookieName = "access-token";

    @Autowired
    private JwtTokenProvider tokenProvider;


    private RedirectStrategy redirectStrateg=new DefaultRedirectStrategy();

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserInterface userInterface;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private JwtAuthenticationFilter jwtAuthenticationFilter;


    private Cookieutill cookieutill=new Cookieutill();

    private static final Logger logger = LoggerFactory.getLogger(AuthContoller.class);

    RedirectStrategy redirectStrategy=new DefaultRedirectStrategy();

    @RequestMapping(value = { "/login" }, method = RequestMethod.POST)
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody login_model loginRequest, HttpServletRequest request, HttpServletResponse response) {

        request.getSession().setAttribute("email",loginRequest.getUsername());
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginRequest.getUsername(),
                        loginRequest.getPassword()
                )
        );
        User user=this.userRepository.findByUsername(loginRequest.getUsername()).get();
//        if (!user.getIsactive())
//            return new ResponseEntity<String>("400",HttpStatus.BAD_REQUEST);

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt ="Bearer "+ tokenProvider.generateToken(authentication);
        cookieutill.create( response,jwtTokenCookieName, jwt, false, 604800);

        return new ResponseEntity<>(jwt, HttpStatus.OK);
    }

    @Autowired
    private JwtTokenProvider getTokenProvider;

    @PreAuthorize("hasRole('USER')")
    @RequestMapping(value = { "/chek-auth" }, method = RequestMethod.GET)
    public ResponseEntity<?> chek_auth(HttpServletRequest request) {
//        String[] s=request.getHeader("Authorization").split(Pattern.quote(" "));
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = { "/register" }, method = RequestMethod.POST)
    public ResponseEntity<?> register(@Valid @RequestBody Auth_model rg) {
        User user=new User(rg.getEmail(),rg.getPassword());
        try {
            this.userInterface.register(user,rg.getRole());
            return new ResponseEntity<>(200, HttpStatus.OK);
        } catch (RException e) {
            return new ResponseEntity<Integer>(e.getStatus(), HttpStatus.BAD_REQUEST);
        }
    }


    @PreAuthorize("hasRole('USER')")
    @RequestMapping(value = { "/logout" }, method = RequestMethod.GET)
    public ResponseEntity<?> logout(HttpServletRequest request,HttpServletResponse response) throws IOException {
        jwtAuthenticationFilter.getJwtFromRequest(request);
        Cookieutill.clear(response, jwtTokenCookieName);
        SecurityContextHolder.getContext().setAuthentication(null);
//        redirectStrateg.sendRedirect(request, response, "https://www.google.com");
        return new ResponseEntity<>( HttpStatus.OK);
    }

//    @RequestMapping(value = "/confirm-account", method =  RequestMethod.GET)
//    public ResponseEntity<?> confirmUserAccount(@RequestParam("token") String confirmationToken, HttpServletResponse response, HttpServletRequest request) throws IOException {
//        ConfirmationToken token = confirmationTokenRepository.findByConfirmationToken(confirmationToken);
//        if (token != null) {
//            if (token.getIsused())
//                return new ResponseEntity<String>("400",HttpStatus.BAD_REQUEST);
//            User person = userRepository.findByUsername(token.getUser().getUsername()).get();
//            person.setIsactive(true);
//            userRepository.save(person);
//            redirectStrategy.sendRedirect(request, response, "http://localhost:3000/?confirm");
//            return new ResponseEntity<>(HttpStatus.OK);
//        }
//        return new ResponseEntity<String>("401",HttpStatus.BAD_REQUEST);
//    }



    ///////////rest-password

//    @RequestMapping(value = "/forget-passrequest", method =  RequestMethod.POST )
//    public ResponseEntity<?> requestnewpass(@RequestParam("email") String email) {
//        try {
//            userInterface.forgetpass(email);
//            return new ResponseEntity<>(HttpStatus.OK);
//        } catch (RException e) {
//            return new ResponseEntity<String>(e.getStatus()+"",HttpStatus.BAD_REQUEST);
//        }
//    }

//    @RequestMapping(value = "/forget-passrequest", method =  RequestMethod.GET)
//    public ResponseEntity<?> Resetpass(@RequestParam("token") String resetpassToken,HttpServletRequest request,HttpServletResponse response) throws IOException {
//        ResetPasswordToken token = resetPasswordTokenRepository.findByResetpassToken(resetpassToken);
//        if (token!=null)
//        {
//            if (token.getIsused())
//                return new ResponseEntity<String>("400",HttpStatus.BAD_REQUEST);
//            token.setIsused(true);
//            resetPasswordTokenRepository.save(token);
//            request.getSession().setAttribute("id",token.getUser().getId());
//
//            //////redirect
//            redirectStrategy.sendRedirect(request, response, "https://localhost:3000");
//
//            return new ResponseEntity<>(HttpStatus.OK);
//        }
//        return new ResponseEntity<String>("401",HttpStatus.OK);
//    }

//    @RequestMapping(value = "/forget-restpass", method = RequestMethod.POST)
//    public ResponseEntity<?> forgetpass(@Valid @Size(min = 9) @RequestParam String password, HttpServletRequest request, HttpServletResponse response) throws IOException {
//
//        if (password.length()<9)
//            return new ResponseEntity<String>("400",HttpStatus.BAD_REQUEST);
//        if (request.getSession().getAttribute("id")!=null)
//        {
//            User user=userRepository.findById((Long)request.getSession().getAttribute("id")).get();
//            PasswordEncoder passwordEncoder=new BCryptPasswordEncoder();
//            user.setPassword(passwordEncoder.encode(password));
//            userRepository.save(user);
//            request.getSession().setAttribute("id",null);
//            redirectStrategy.sendRedirect(request, response, "https://localhost:3000");
//            return new ResponseEntity<>(HttpStatus.OK);
//        }
//        return new ResponseEntity<String>("401",HttpStatus.BAD_REQUEST);
//    }

}
