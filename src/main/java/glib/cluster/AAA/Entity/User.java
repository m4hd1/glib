package glib.cluster.AAA.Entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Getter
@Setter
@ToString

@Entity
@Table(name = "user")
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;


	@NotBlank
	@Size(max = 100)
	@Email
	private String username;

	@NotBlank
	@Size(max = 100)
	private String password;

	@Column
	private Boolean isactive=false;

	@ManyToOne
	@JoinColumn(name = "role_id",nullable = false)
	private Role role;

    public User() {

	}

	public User(String username, String password) {
		this.setUsername(username);
		this.setPassword(password);

	}

}
