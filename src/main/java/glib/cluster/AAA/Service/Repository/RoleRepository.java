package glib.cluster.AAA.Service.Repository;

import glib.cluster.AAA.Entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long> {
}
