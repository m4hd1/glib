package glib.cluster.AAA.Service.Interface;




import glib.cluster.AAA.Common.RException;
import glib.cluster.AAA.Entity.User;

import javax.transaction.Transactional;

public interface UserInterface {



    @Transactional
    User register(User user,Integer role)  throws RException;


    @Transactional
    User emailconfirm(String email) throws RException;


    @Transactional
    User forgetpass(String email) throws RException;


}
