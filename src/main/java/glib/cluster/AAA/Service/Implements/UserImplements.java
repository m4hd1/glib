package glib.cluster.AAA.Service.Implements;

import glib.cluster.AAA.Common.RException;
import glib.cluster.AAA.Entity.User;
import glib.cluster.AAA.Service.Interface.UserInterface;
import glib.cluster.AAA.Service.Repository.RoleRepository;
import glib.cluster.AAA.Service.Repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;


@Service
public class UserImplements implements UserInterface {


	@Autowired
	private UserRepository usrep;

	@Autowired
	private RoleRepository roleRepository;

	@Autowired
	PasswordEncoder passwordEncoder;



	@Transactional
	@Override
	public User register(User user,Integer role) throws RException {
		if (this.usrep.findByUsername(user.getUsername()).isPresent())
			throw new RException(404);

		user.setPassword(passwordEncoder.encode(user.getPassword()));
		if(roleRepository.findById((long) role).get()==null)
			throw new RException(405);
		user.setRole(roleRepository.findById((long) role).get());
		user.setIsactive(true);
		user=usrep.save(user);
//		ConfirmationToken confirmationToken = new ConfirmationToken(user);
//
//		ConfirmationTokenRepository.save(confirmationToken);
//
//		SimpleMailMessage mailMessage = new SimpleMailMessage();
//		mailMessage.setTo(user.getUsername());
//		mailMessage.setSubject("Complete Registration!");
//		mailMessage.setFrom("rewundity@gmail.com");
//		mailMessage.setText("To confirm your account, please click here : "+
//				Domain.domain+"auth/confirm-account?token=" + confirmationToken.getConfirmationToken());
//		emailSenderService.sendEmail(mailMessage);
		return user;
	}


    @Transactional
	@Override
	public User emailconfirm(String confirmationToken) throws RException
	{
//		ConfirmationToken token = ConfirmationTokenRepository.findByConfirmationToken(confirmationToken);
//		if (token != null)
//		{
//			User person = usrep.findByUsername(token.getUser().getUsername()).get();
//			person.setIsactive(true);
//			token.setIsused(true);
//			usrep.save(person);
//			ConfirmationTokenRepository.save(token);
//			return person;
//		}
//		throw new RException(201);
		return null;
	}


    @Transactional
	@Override
	public User forgetpass(String email) throws RException
	{
//		Optional<User> person = usrep.findByUsername(email);
//		if(!person.isPresent())
//			throw new RException(400);
//		ResetPasswordToken resetpassToken = new ResetPasswordToken(person.get());
//		resetpassToken.setIsused(false);
//		ResetPasswordTokenRepository.save(resetpassToken);
//		SimpleMailMessage mailMessage = new SimpleMailMessage();
//		mailMessage.setTo(person.get().getUsername());
//		mailMessage.setSubject("Forget password email");
//		mailMessage.setFrom("rewundity@gmail.com");
//		mailMessage.setText("To get new pass Click here " + Domain.domain+"auth/forget-passrequest?token="
//				+ resetpassToken.getResetpassToken());
//		emailSenderService.sendEmail(mailMessage);
//		return person.get();
		return null;
	}

}
