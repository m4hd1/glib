package glib.cluster.AAA.security;


import glib.cluster.AAA.Entity.User;
import glib.cluster.AAA.Service.Repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

/**
 * Created by rajeevkumarsingh on 07/12/17.
 */
@Component
public class JwtAuthenticationEntryPoint implements AuthenticationEntryPoint {

    @Autowired
    private UserRepository userRepository;

    @Override
    public void commence(HttpServletRequest httpServletRequest,
                         HttpServletResponse httpServletResponse,
                         AuthenticationException e) throws IOException, ServletException {
        String email= (String) httpServletRequest.getSession().getAttribute("email");
        httpServletRequest.getSession().setAttribute("email",null);
        Optional<User> user=userRepository.findByUsername(email);
        if (!user.isPresent()) {
            httpServletResponse.sendError(HttpServletResponse.SC_BAD_REQUEST, "401");
            return;
        }
        httpServletResponse.sendError(HttpServletResponse.SC_BAD_REQUEST,"402");
    }
}
